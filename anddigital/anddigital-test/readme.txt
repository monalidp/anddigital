Swagger-UI URL on localhost - localhost:8080/anddigital-test/swagger-ui.html

API Endpoints on localhost - 
localhost:8080/anddigital-test/api/phones
localhost:8080/anddigital-test/api/customers/{customerId}/phones
localhost:8080/anddigital-test/api/phones/{phoneId}/activate

Please note - There is currently no API endpoint to add new phones. However the PhoneService has this method to facilitate the testing
