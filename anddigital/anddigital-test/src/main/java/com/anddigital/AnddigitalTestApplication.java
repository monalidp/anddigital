package com.anddigital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnddigitalTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnddigitalTestApplication.class, args);
	}
}
