package com.anddigital.service;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.anddigital.model.Phone;

@Service
public class PhoneService {

	// Ideally this should come from database.
	private Set<Phone> phones;

	public PhoneService() {
		phones = new HashSet<>();
	}

	public Set<Phone> getAllPhones() {
		return phones;
	}

	public void addPhone(Phone phone) {
		phones.add(phone);
	}

	public Set<Phone> findByCustomerId(Long customerId) {
		return phones.stream().filter(phone -> phone.getCustomerId().equals(customerId)).collect(Collectors.toSet());
	}

	public Phone activatePhone(Phone inputPhone) {
		Phone phone = phones.stream().filter(ph -> ph.getId().equals(inputPhone.getId())).findFirst().orElse(null);
		if (phone != null) {// if found, activate
			phone.setActive(Boolean.TRUE);
		}
		return phone;
	}

}
