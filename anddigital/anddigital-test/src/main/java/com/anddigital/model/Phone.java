package com.anddigital.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Phone implements Serializable{

	private static final long serialVersionUID = 1L;

	//primary key
	private Long id;
	
	private String phoneNumber;
	
	private Boolean active;
	
	//Associated customer (Many-to-One)
	private Long customerId;
		
}
