package com.anddigital.api;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.anddigital.model.Phone;
import com.anddigital.service.PhoneService;

@RestController
@RequestMapping("api")
public class CustomerController {
	
	@Autowired
	PhoneService service;
	

	// Find Phones for a customer
	@GetMapping("customers/{customerId}/phones")
	public ResponseEntity<Set<Phone>> findByCustomerId(@PathVariable("customerId") Long customerId) {
		Set<Phone> phones = service.findByCustomerId(customerId);
		return new ResponseEntity<>(phones, HttpStatus.OK);
	}

}
