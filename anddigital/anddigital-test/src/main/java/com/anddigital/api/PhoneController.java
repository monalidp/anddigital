package com.anddigital.api;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.anddigital.model.Phone;
import com.anddigital.service.PhoneService;

@RestController
@RequestMapping("api")
public class PhoneController {

	@Autowired
	PhoneService service;

	// Get All Phones
	@GetMapping("phones")
	public ResponseEntity<Set<Phone>> getAllPhones() {
		Set<Phone> phones = service.getAllPhones();
		return new ResponseEntity<>(phones, HttpStatus.OK);
	}

	// Find Phones for a customer
	@GetMapping("phones/{customerId}")
	public ResponseEntity<Set<Phone>> findByCustomerId(@PathVariable("customerId") Long customerId) {
		Set<Phone> phones = service.findByCustomerId(customerId);
		return new ResponseEntity<>(phones, HttpStatus.OK);
	}

	// activate a phone
	@PutMapping("phones/{phoneId}/activate")
	public ResponseEntity<Phone> activatePhone(@PathVariable("phoneId") Long phoneId) {
		Phone inputPhone = Phone.builder().id(phoneId).build();
		Phone outputPhone = service.activatePhone(inputPhone);
		if (outputPhone != null) {
			return new ResponseEntity<>(outputPhone, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(outputPhone, HttpStatus.NOT_FOUND);
		}
	}

}
