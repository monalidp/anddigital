package com.anddigital.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import com.anddigital.model.Phone;

@RunWith(SpringRunner.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class PhoneServiceTest {
	
	@TestConfiguration
    static class PhoneServiceTestContextConfiguration {
  
        @Bean
        public PhoneService phoneService() {
            return new PhoneService();
        }
    }

	@Autowired
	PhoneService service;
	
	@Test
	public void test_getAllPhones_noResults() {
		Set<Phone> phones = service.getAllPhones();
		
		assertEquals(0,phones.size());
	}
	
	@Test
	public void test_getAllPhones_withResults() {
		Phone phone = Phone.builder().id(1L).customerId(1L).phoneNumber("07766554433").active(Boolean.TRUE).build();
		service.addPhone(phone);
		Set<Phone> phones = service.getAllPhones();
		
		assertEquals(1,phones.size());
	}
	
	@Test
	public void test_findByCustomerId_noResults() {		
		Set<Phone> phones = service.findByCustomerId(1L);
		
		assertEquals(0,phones.size());
	}
	
	@Test
	public void test_findByCustomerId_withResults() {
		Phone phone = Phone.builder().id(1L).customerId(1L).phoneNumber("07766554433").active(Boolean.TRUE).build();
		service.addPhone(phone);
		Set<Phone> phones = service.findByCustomerId(1L);
		
		assertEquals(1,phones.size());
	}
	
	@Test
	public void test_addPhone() {
		Phone phone = Phone.builder().id(1L).customerId(1L).phoneNumber("07766554433").active(Boolean.TRUE).build();
		assertEquals(0,service.getAllPhones().size());
		service.addPhone(phone);
		assertEquals(1,service.getAllPhones().size());
	}
	
	@Test
	public void test_activatePhone_success() {
		Phone phone = Phone.builder().id(1L).customerId(1L).phoneNumber("07766554433").active(Boolean.FALSE).build();
		service.addPhone(phone);
		assertEquals(1,service.getAllPhones().size());
		
		Phone inputPhone =  Phone.builder().id(1L).build();
		Phone outputPhone = service.activatePhone(inputPhone);
		
		assertTrue("Phone not found", outputPhone != null);
		assertTrue("Activation failed", outputPhone.getActive());
	}
	
	@Test
	public void test_activatePhone_failure() {
		Phone phone = Phone.builder().id(1L).customerId(1L).phoneNumber("07766554433").active(Boolean.FALSE).build();
		service.addPhone(phone);
		assertEquals(1,service.getAllPhones().size());
		
		Phone inputPhone =  Phone.builder().id(2L).build();
		Phone outputPhone = service.activatePhone(inputPhone);
		
		assertTrue(outputPhone == null); //No phone exists for given id
	
	}
}
