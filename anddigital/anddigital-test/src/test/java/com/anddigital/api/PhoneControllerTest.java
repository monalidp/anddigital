package com.anddigital.api;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.anddigital.model.Phone;
import com.anddigital.service.PhoneService;

@RunWith(SpringRunner.class)
@WebMvcTest(PhoneController.class)
public class PhoneControllerTest {
	
	@Autowired
	MockMvc mvc;
	
	@MockBean
	PhoneService service;
	
	@Test
	public void test_getAllPhones_noResults() throws Exception {
		mvc.perform(get("/api/phones")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$", hasSize(0)));
			    
	}
	
	@Test
	public void test_getAllPhones_withResults() throws Exception {
		when(service.getAllPhones()).thenReturn(getPhones());
		mvc.perform(get("/api/phones")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$", hasSize(3)));
			    
	}
	
	@Test
	public void test_activatePhone_failure() throws Exception {
		when(service.activatePhone(any())).thenReturn(null);
		mvc.perform(put("/api/phones/10/activate")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isNotFound());
		Mockito.verify(service, times(1)).activatePhone(any(Phone.class));
	}
	
	@Test
	public void test_activatePhone_success() throws Exception {
		when(service.activatePhone(any())).thenReturn(Phone.builder().id(1L).customerId(1L).phoneNumber("1111111111").active(true).build());
		mvc.perform(put("/api/phones/1/activate")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk());
		Mockito.verify(service, times(1)).activatePhone(any(Phone.class));
	}
		
	private Set<Phone> getPhones() {
		Set<Phone> phones = new HashSet<>();
		//Inactive phone for cust 1
		phones.add(Phone.builder().id(1L).customerId(1L).phoneNumber("1111111111").active(false).build());
		
		//Multiple phones for cust 2
		phones.add(Phone.builder().id(2L).customerId(2L).phoneNumber("2222222222").active(true).build());
		phones.add(Phone.builder().id(3L).customerId(2L).phoneNumber("3333333333").active(true).build());
		
		return phones;
	}

}
