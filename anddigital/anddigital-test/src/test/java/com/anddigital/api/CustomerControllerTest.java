package com.anddigital.api;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.anddigital.model.Phone;
import com.anddigital.service.PhoneService;

@RunWith(SpringRunner.class)
@WebMvcTest(CustomerController.class)
public class CustomerControllerTest {

	@Autowired
	MockMvc mvc;

	@MockBean
	PhoneService service;

	@Test
	public void test_findByCustomerId_noResults() throws Exception {
		when(service.getAllPhones()).thenReturn(getPhones());
		mvc.perform(get("/api/customers/5/phones").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(0)));

	}

	@Test
	public void test_findByCustomerId_withResults() throws Exception {
		when(service.getAllPhones()).thenReturn(getPhones());
		mvc.perform(get("/api/customers/2/phones").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)));

	}

	private Set<Phone> getPhones() {
		Set<Phone> phones = new HashSet<>();
		// Inactive phone for cust 1
		phones.add(Phone.builder().id(1L).customerId(1L).phoneNumber("1111111111").active(false).build());

		// Multiple phones for cust 2
		phones.add(Phone.builder().id(2L).customerId(2L).phoneNumber("2222222222").active(true).build());
		phones.add(Phone.builder().id(3L).customerId(2L).phoneNumber("3333333333").active(true).build());

		return phones;
	}

}
